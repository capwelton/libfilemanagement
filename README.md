## Librairie pour effectuer des manipulations sur les fichiers ##

Bibliothèque d'Ovidentia pour effectuer des traitements avec des fichiers :

* Génération de fichiers Excel
* Génération d'images : redimensionnement (thumbnail : miniature d'image), recadrage, stylisation
* ...