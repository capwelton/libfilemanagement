<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
 

require_once dirname(__FILE__).'/functions.php';



/**
 * Multiple file upload, a replacement for the input html field
 */
class Func_FileListUploader extends bab_functionality
{
	
	/**
	 * File list unique identifier
	 * @access protected
	 */
	var $uid;
	
	/**
	 * Get description of functionality
	 * @return string
	 */
	function getDescription() {
		return lfm_translate('Multiple file upload');
	}

	/**
	 * Define the uid used for storage in ovidentia
	 * The uid must be unique by field
	 * @param	string	$uid
	 * @return 	boolean	
	 */
	function setListUid($uid) {
	
		if (!preg_match('/\w+/', $uid)) {
			trigger_error('UID must contain only alphanumeric caracters');
			return false;
		}
	
	
		$this->uid = $uid;
		return true;
	}
	
	
	/**
	 * List of files
	 * @return array
	 */
	function getFiles() {
		return lfm_getFiles($this->uid);
	}
	
	
	/**
	 * Get path to file or false if file does not exists
	 * @param	string	$filename
	 * @return false|string
	 */
	function getFilePath($filename) {
	
		$file = lfm_getDirectory($this->uid).'/'.$filename;
		
		if (!is_readable($file)) {
			return false;
		}
	
		return $file;
	}
	
	
	/**
	 * Add a file for current UID
	 * The parameter is a bab_fileHandler object, to upload, move or copy a file
	 * @see		ovidentia/utilit/uploadincl.php
	 * @param	bab_fileHandler	$fileObj
	 */
	function addFile($fileObj) {
		$fileObj->import(lfm_getDirectory($this->uid).'/'.$fileObj->filename);
	}
	
	
	/**
	 * This method return a string to download the file as atachement
	 * if the file is not loaded in the system, the method return false
	 *
	 * @param	string	$filename	filename in the list to download
	 *
	 * @return false|string
	 */
	function getDownloadUrl($filename) {
		$addon = bab_getAddonInfosInstance('LibFileManagement');
		
		if (false === $addon) {
			return false;
		}
		
		if (false === $this->getFilePath($filename)) {
			return false;
		}
		
		require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
		
		$url = $addon->getUrl().'download';
		$url = bab_url::mod($url, 'uid', $this->uid);
		$url = bab_url::mod($url, 'filename', $filename);
		
		return $url;
	}
	
	/**
	 * Delete file stored for the uid
	 */
	function deleteAllFiles() {
		lfm_emptyDirectory($this->uid);
	}
	
	/**
	 * Delete file stored for the uid
	 * @return boolean
	 */
	function deleteFile($filename) {
		$filepath = $this->getFilePath($filename);
		return unlink($filepath);
	}
	
	
	/**
	 * The method return some html to display the field
	 * @return string
	 */
	function getHtml($attributes = array()) {
	
		$addon = bab_getAddonInfosInstance('LibFileManagement');
		require_once $addon->getPhpPath().'genericuploader.php';
		
		return lfm_genericUploader($this->uid, get_class($this), true, $attributes);
	}
	
	/**
	 * The method return a widget object or false if the widgets addons is not installed
	 * @return	false|lfm_WidgetFileUploader
	 */
	function getWidget() {
		
		$func = @bab_functionality::get('Widgets');
		if (false === $func) {
			return false;
		}
		
		return $func->Create('Widget_Html', $this->getHtml());
	}

}

