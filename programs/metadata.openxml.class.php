<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/xmlinarchiveincl.php';
require_once dirname(__FILE__).'/metadata.class.php';



class lfm_OpenXmlMetadataParser extends lfm_XmlInArchiveMetadataParser {




	/**
	 * Parse metadata
	 */
	public function parse() {

		if ($this->parsed) {
			return true;
		}

		if (!$this->isValid()) {
			return false;
		}

		$filepath = $this->filepath;

		$reader = new XMLReader();

		if (!@$reader->open('zip://' . $filepath . '#docProps/core.xml')) {
			return false;
		}

		$this->meta = array();
		while ($reader->read()) {
			if ($reader->nodeType == XMLREADER::ELEMENT) {
				$elm = $reader->name;
			} else {
				if ($reader->nodeType == XMLREADER::END_ELEMENT && $reader->name == 'cp:coreProperties') {
					break;
				}
				if (!trim($reader->value)) {
					continue;
				}

				$value = bab_getStringAccordingToDataBase($reader->value, 'UTF-8');
				if (isset($this->meta[$elm])) {
					$this->meta[$elm] .= ', '.$value;
				} else {
					$this->meta[$elm] = $value;
				}
			}
		}


		if (!$reader->open('zip://' . $filepath . '#docProps/app.xml')) {
			return false;
		}

		while ($reader->read()) {
			if ($reader->nodeType == XMLREADER::ELEMENT) {
				$elm = $reader->name;
			} else {
				if ($reader->nodeType == XMLREADER::END_ELEMENT && $reader->name == 'Properties') {
					break;
				}
				if (!trim($reader->value)) {
					continue;
				}

				$value = bab_getStringAccordingToDataBase($reader->value, 'UTF-8');
				if (isset($this->meta[$elm])) {
					$this->meta[$elm] .= ', '.$value;
				} else {
					$this->meta[$elm] = $value;
				}
			}
		}

		$this->parsed = true;
	}
}



class lfm_OpenXmlDocMetadata extends lfm_MetadataNamespace_Doc {

	private $parsed = false;


	public function getAllMeta() {

		$parser = bab_getInstance('lfm_OpenXmlMetadataParser');

		if ($parser->isValid()) {

			return array(
				'Title',
				'Subject',
				'Keywords',
				'Created',
				'Author',
				'PageCount',
				'WordCount'
			);

		}

		return array();
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {
		if (false === $this->parsed) {
			$this->parsed = true;
			$parser = bab_getInstance('lfm_OpenXmlMetadataParser');
			$parser->setFilePath($this->getFilePath());
			$parser->parse();
			
			
			$Title 		= isset($parser->meta['dc:title']) 				? $parser->meta['dc:title'] : '';
			$Subject	= isset($parser->meta['dc:subject']) 			? $parser->meta['dc:subject'] : '';
			$Keywords	= isset($parser->meta['cp:keywords']) 			? $parser->meta['cp:keywords'] : '';
			$Author		= isset($parser->meta['dc:creator']) 			? $parser->meta['dc:creator'] : '';
			$PageCount	= isset($parser->meta['Pages']) 				? $parser->meta['Pages'] : '';
			$WordCount	= isset($parser->meta['Words']) 				? $parser->meta['Words'] : '';
			$Created	= isset($parser->meta['dcterms:created']) 		? $parser->meta['dcterms:created'] : '';

			$this->setMeta('Title'		, $Title);
			$this->setMeta('Subject'	, $Subject);
			$this->setMeta('Keywords'	, $Keywords);
			$this->setMeta('Created'	, $Created);
			$this->setMeta('Author'		, $Author);
			$this->setMeta('PageCount'	, $PageCount);
			$this->setMeta('WordCount'	, $WordCount);
		}

		return $this->getStoredValue($name);
	}

}
















class lfm_OpenXmlFileMetadata extends lfm_MetadataNamespace_File {

	private $parsed = false;


	public function getAllMeta() {

		$parser = bab_getInstance('lfm_OpenXmlMetadataParser');

		if ($parser->isValid()) {

			return array(
				'Created',
				'Publisher',
				'Modified',
				'Description'
			);

		}

		return array();
	}


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	public function getMetaValue($name) {
		if (false === $this->parsed) {
			$this->parsed = true;
			$parser = bab_getInstance('lfm_OpenXmlMetadataParser');
			$parser->setFilePath($this->getFilePath());
			$parser->parse();

			$Created		= isset($parser->meta['dcterms:created']) 	? $parser->meta['dcterms:created'] : '';
			$Publisher		= isset($parser->meta['cp:lastModifiedBy']) ? $parser->meta['cp:lastModifiedBy'] : '';
			$Modified		= isset($parser->meta['dcterms:modified']) 	? $parser->meta['dcterms:modified'] : '';
			$Description 	= isset($parser->meta['dc:description']) 	? $parser->meta['dc:description'] : '';

			$this->setMeta('Created'		, $Created);
			$this->setMeta('Publisher'		, $Publisher);
			$this->setMeta('Modified'		, $Modified);
			$this->setMeta('Description'	, $Description);
		}

		return $this->getStoredValue($name);
	}

}

