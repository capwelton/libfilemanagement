<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

/**
 * @param	string	$uid
 * @param	string	$classname
 * @param	boolean	$multiple
 */
function lfm_genericUploader($uid, $classname, $multiple, $attributes=array()) {
	
	
	
	global $babBody;
	$addon = bab_getAddonInfosInstance('LibFileManagement');

	if (!class_exists('lfm_genericUploaderObj')) {
		class lfm_genericUploaderObj {}
	}
	$temp = new lfm_genericUploaderObj();

	require_once $GLOBALS['babInstallPath'].'utilit/urlincl.php';
	
	$tocken = uniqid();
	$_SESSION['LibFileManagement']['FileUploader'][$tocken] = array(
		'uid' 			=> $uid,
		'classname' 	=> $classname,
		'multiple' 		=> $multiple
	);
	
	$_SESSION['BAB_SESS_WSUSER'] = 1;
	
	$url = $addon->getUrl().'upload';
	$url = bab_url::mod($url,'idx','iframe_generic');
	$url = bab_url::mod($url,'tocken', $tocken);
	
	$temp->frameurl = bab_toHtml($url);

	$template = $multiple ? 'field_multiple' : 'field_simple';

	$temp->frm_height	= ($template == 'field_multiple'? '300': '150');
	$temp->frm_width	= '400';

	if( isset($attributes))
	{
		if(isset($attributes['width']))
		{
			$temp->frm_width = $attributes['width'];
		}
		if(isset($attributes['height']))
		{
			$temp->frm_height = $attributes['height'];
		}
	}

	
	return bab_printTemplate($temp, $addon->getRelativePath().'uploader.html', $template);

}
