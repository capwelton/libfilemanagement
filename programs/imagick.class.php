<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * This file provides compatibility classes: lfm_Ffmpeg_Frame and lfm_Ffmpeg_Movie which
 * should be almost compatible with ffmpeg_movie and ffmpeg_frame (defined in the ffmpeg php extension).
 * Those compatibility classes use the command line ffmpeg tool.
 *
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';


if (class_exists('Imagick')) {

	class lfm_Imagick extends Imagick { }

} else {

	/**
	 * A class compatible with Imagick but not for all methods, using the command line imagemagick tool or evince-thumbnailer.
	 */
	class lfm_Imagick
	{

		private $filename = null;

		private $write_size = array();
		private $fit = true;

		/**
		 * executable, default is linux version
		 */
		private $convertExecutable = 'convert';


		public function __construct($filename)
		{
			$this->filename = $filename;

			if ('\\' === DIRECTORY_SEPARATOR) {
				$this->convertExecutable = null;
			}
		}

		/**
		 * Merges a sequence of images. This is useful for combining Photoshop layers into a single image.
		 */
		public function flattenImages()
		{
			return $this;
		}


		/**
		 * @param	int		$width
		 * @param	int		$height
		 * @param	bool	$fit		defaut value is false, the fit param is not effective for the moment, the result is always true
		 */
		public function scaleImage($width, $height, $fit = false)
		{
			$this->write_size['width'] 	= $width;
			$this->write_size['height'] = $height;
			$this->fit = true;
		}


		/**
		 * Write image
		 * @param string $destination
		 * @return bool
		 */
		public function writeImage($destination)
		{
		    $retval = null;

		    if ($this->convertExecutable) {
    			if (empty($this->write_size)) {
    				$geometry = '';
    			} else {
    				$geometry = '-geometry ' . $this->write_size['width'] . 'x' . $this->write_size['height'];
    			}

    			@lfm_system($this->convertExecutable.' -background none "' . $this->filename . '" '.$geometry.' "' . $destination . '"', $retval);
		    }

			if ($retval !== 0) {

				// convert probably not present, try with evince-thumbnailer
				// should work if the source file is a PS or PDF file

				$filename = rtrim($this->filename, ' [0]');

				$sparam = '';
				if (!empty($this->write_size)) {
				    $sparam = '-s '.$this->write_size['height'];
				}

				lfm_system('evince-thumbnailer '.$sparam.' "' .$filename. '" "' .$destination. '"', $retval);

				if ($retval !== 0) {
					// evince failed
					return false;
				}

				if ($this->fit) {
					// evince-thumbnailer do not fit the image, resize with GD if necessary

					$imageInfo = @getImageSize($destination);
					lfm_createGdThumbnail($destination, $imageInfo, $this->write_size['width'], $this->write_size['height'] , basename($destination), array());
				}
			}

			return true;
		}


		/**
	  	 * Clears all resources associated to Imagick object
		 */
		public function clear()
		{

		}
	}
}
