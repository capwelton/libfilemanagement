<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/thumbnail_ovml.php';



/**
 * Display an image in HTML
 * same as OFThumbnail but output a img tag and accept more input attribute
 *
 * src: the source image remain untouched, if empty <OFImg> return empty string
 * ovsrc: set image src with an ovml variable name
 * unknown attributes will be reported on the img tag
 *
 * <OFImg
 * 		( src="" | ovsrc="" | id_directory_entry="" | id_user="" | id_article="" | path="absolute/path/to/image" )
 * 		[maxwidth="400"]
 * 		[maxheight="400"]
 * 		[resizemode="KEEP_ASPECT_RATIO | CROP_CENTER"]
 * 		[default="path/to/default/image.jpg"]
 * 		[innerborderwidth="0"]
 * 		[innerbordercolor="#ffffff"]
 * 		[innerborder="0,#ffffff"]
 * 		[outerborderwidth="0"]
 * 		[outerbordercolor="#ffffff"]
 * 		[outerborder="0,#ffffff"]
 *      [resizeleft="0.0 .. 1.0"]
 *      [resizetop="0.0 .. 1.0"]
 * >
 *
 */
class Func_Ovml_Function_Img extends Func_Ovml_Function_Thumbnail
{
    /**
     * Render output of the ovml function
     * @param string $imageUrl          This is the url from the thumbnailer if sourceMethod is
     *                                  id_directory_entry, id_user, id_article or path
     * @param string $sourceMethod
     * @param string $sourceValue
     *
     */
    protected function renderOutput($imageUrl, $sourceMethod, $sourceValue)
    {
        if ('src' === $sourceMethod) {
            $imageUrl = $sourceValue;
        }

        if ('ovsrc' === $sourceMethod) {
            if (!empty($sourceValue)) {
                $imageUrl = $this->gctx->get($sourceValue);
            }
        }

        if ('' === (string) $imageUrl) {
            return '';
        }

        $htmlAttributes = array();
        foreach ($this->externalAttributes as $key => $value) {
            $htmlAttributes[] = bab_toHtml($key).'="'.bab_toHtml($value).'"';
        }
        if (isset($this->computedWidth)) {
            $htmlAttributes[] = 'width="' . $this->computedWidth. '"';
        }
        if (isset($this->computedHeight)) {
            $htmlAttributes[] = 'height="' . $this->computedHeight . '"';
        }

        return '<img src="'.bab_toHtml($imageUrl).'" '.implode(' ', $htmlAttributes).' />';
    }
}