<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/functions.php';




/**
 * Thumbnailer
 * Support files types :
 * <ul>
 * 	<li>open document format : odt, odp</li>
 *  <li>office open xml : docx, pptx</li>
 * 	<li>images : jpeg, png, gif</li>
 * 	<li>documents : pdf, svg (imagemagick required)</li>
 * 	<li>videos : avi, wmv, mov ... (ffmpeg required)</li>
 * </ul>
 */
class Func_Thumbnailer extends bab_functionality {

    protected $filePath 		= null;
    protected $fileData			= null;
    protected $effect_options	= array();

    protected $computedWidth = null;
    protected $computedHeight = null;
    protected $thumbfilename = null;
    
    const KEEP_ASPECT_RATIO		= 1;
    const CROP_CENTER			= 2;

    /**
     * Get description of functionality
     * @return string
     */
    function getDescription() {
        return lfm_translate('Get a thumbnail for a file, if possible');
    }


    /**
     * @return int | null
     */
    public function getComputedWidth()
    {
        if (!isset($this->computedWidth) && isset($this->thumbfilename)) {
            list($this->computedWidth, $this->computedHeight) = @getImageSize(lfm_getThumbnailBasePath() . $this->thumbfilename);
        }
        return $this->computedWidth;
    }
    
    /**
     * @return int | null
     */
    public function getComputedHeight()
    {
        if (!isset($this->computedHeight) && isset($this->thumbfilename)) {
            list($this->computedWidth, $this->computedHeight) = @getImageSize(lfm_getThumbnailBasePath() . $this->thumbfilename);
        }
        return $this->computedHeight;
    }
    
    /**
     *
     */
    protected function setDefaultOptions()
    {
        $this->effect_options = array(
            'resizemode' => Func_Thumbnailer::KEEP_ASPECT_RATIO,
            'imageformat' => 'auto'
        );
    }

    /**
     * Set source image from a file
     * @param	string | bab_path	$filePath	Full path to a readable file
     */
    public function setSourceFile($filePath)
    {
        if ($filePath instanceOf bab_Path) {
            $filePath = $filePath->toString();
        }
        $this->computedWidth = null;
        $this->computedHeight = null;

        $this->filePath = $filePath;
        $this->fileData = null;
        $this->setDefaultOptions();
    }

    /**
     * Set source image from data, this method to create thumbnail work only with images of types managed by the GD library.
     * @param	string	$data
     * @param	string	$lastupdate		ISO datetime
     */
    public function setSourceBinary($data, $lastupdate)
    {
        $this->fileData = array(
            'data' => $data,
            'lastupdate' => $lastupdate
        );
        $this->computedWidth = null;
        $this->computedHeight = null;
        
        $this->filePath = null;

        $this->setDefaultOptions();
    }


    /**
     * Returns an array of available effects that can be applied to a thumbnailed image.
     *
     * @return array
     */
    public function availableEffects()
    {
        $availableEffects = array(
            'setDropShadow' 		=> 'setDropShadow',
            'setBackgroundColor' 	=> 'setBackgroundColor',
            'setBorder' 			=> 'setBorder',
            'setPolaroid' 			=> 'setPolaroid',
            'setResizeMode'			=> 'setResizeMode',
            'setImageFormat'		=> 'setImageFormat'
        );

        return $availableEffects;
    }



    /**
     * Checks if the specified effect exists and can be applied to the thumbnailed image.
     *
     * @param string $effect The name of the effect.
     * @return bool
     */
    public function isEffectAvailable($effect)
    {
        $availableEffects = $this->availableEffects();
        return isset($availableEffects[$effect]);
    }



    /**
     * Activate drop shadow with optional parameters
     * If imagemagick is available, all options will be used
     * If drop shadows are generated by GD library, only $backgroundcolor is valid
     *
     * @param	string	[$shadowcolor]			color in hexadecimal like #000000
     * @param	string	[$backgroundcolor]		color in hexadecimal like #FFFFFF
     * @param	int		[$percentopacity]		opacity of shadow
     * @param	int		[$offset]				distance between image and shadow in bottom right direction
     */
    function setDropShadow($shadowcolor = '#000000', $backgroundcolor = '#FFFFFF', $percentopacity = 60, $offset = 5) {

        $this->effect_options['method']				= 'addDropShadow';
        $this->effect_options['shadowcolor']		= $shadowcolor;
        $this->effect_options['backgroundcolor']	= $backgroundcolor;
        $this->effect_options['percentopacity']		= $percentopacity;
        $this->effect_options['offset']				= $offset;

    }



    /**
     * Fill background with a color to prevent transparency (default white)
     * @param	string	$color
     */
    public function setBackgroundColor($color = '#FFFFFF') {

        $this->effect_options['method']			= 'addBorder';
        $this->effect_options['size'] 			= 0;
        $this->effect_options['color']			= $color;
        $this->effect_options['inner_size']		= 0;
        $this->effect_options['inner_color']	= $color;

    }




    /**
     * Add a border to thumbnail
     *
     * @param	int		$size			Size in pixel of outer border
     * @param	string	$color			Color of outer border
     * @param	int		$inner_size		Size of inner margin
     * @param	string	$color			Color of inner border
     */
    public function setBorder($size = 1, $color = '#000000', $inner_size = 2, $inner_color = '#FFFFFF') {

        $this->effect_options['method']			= 'addBorder';
        $this->effect_options['size'] 			= $size;
        $this->effect_options['color']			= $color;
        $this->effect_options['inner_size']		= $inner_size;
        $this->effect_options['inner_color'] 	= $inner_color;

    }



    /**
     * Activate polaroid mode for thumbnail
     * work only if imagemagik is available, if not available it fallback to regular drop shadow
     *
     * @param	string		[$backgroundcolor]	background color
     */
    public function setPolaroid($angle = false, $bordercolor = '#FFFFFF', $shadowcolor = '#000000', $backgroundcolor = '#FFFFFF') {

        $this->effect_options['method']				= 'setPolaroid';
        $this->effect_options['bordercolor'] 		= $bordercolor;
        $this->effect_options['shadowcolor'] 		= $shadowcolor;
        $this->effect_options['backgroundcolor']	= $backgroundcolor;
        $this->effect_options['angle']				= $angle;
    }

    /**
     *
     * @param int      $resizemode	Func_Thumbnailer::KEEP_ASPECT_RATIO | Func_Thumbnailer::CROP_CENTER
     * @param float    $resizeleft For crop_center, value from 0 => left to 1 => right.
     * @param float    $resizetop  For crop_center, value from 0 => top to 1 => bottom.
     */
    public function setResizeMode($resizemode, $resizeleft = 0.5, $resizetop = 0.5)
    {
        $this->effect_options['resizemode'] = $resizemode;
        $this->effect_options['resizeleft'] = $resizeleft;
        $this->effect_options['resizetop'] = $resizetop;
        return $this;
    }


    /**
    *
    * @param string $imageFormat 'png' or
    */
    public function setImageFormat($imageFormat)
    {
        $this->effect_options['imageformat'] = $imageFormat;
        return $this;
    }

    /**
     * /!\ Rotate is done first
     * crop a specific image area
     *
     * @param int		$x
     * @param int		$y
     * @param int		$width
     * @param int		$height
     * @param float		$rotate
     */
    public function setCropArea($x, $y, $width, $height, $rotate = 0)
    {
        $this->effect_options['croparea'] = array(
            'x' => $x,
            'y' => $y,
            'width' => $width,
            'height' => $height,
            'rotate' => $rotate
        );

        return $this;
    }



    /**
     * Get The resized image
     *
     * @param	int				$width
     * @param	int				$height
     * @return string | null | false		the thumb file name or null if the ressource of original file cannot be found or false if the thumbnail cannot be created.
     */
    public function getThumbnail($width, $height)
    {
        require_once dirname(__FILE__).'/thumbnail.php';

        $thumbfilename = lfm_getThumbnail($this->filePath, $this->fileData, $width, $height, $this->effect_options);
        $this->thumbfilename = $thumbfilename;
        
        if (false === $thumbfilename) {
            bab_debug(sprintf('cannot write thumbnail image file in %s', lfm_getThumbnailBaseUrl()), DBG_WARNING, 'thumbnailer');
            return false;
        }

        if (null === $thumbfilename) {
            bab_debug(sprintf('cannot open original image file %s', $this->filePath), DBG_WARNING, 'thumbnailer');
            return null;
        }

        return lfm_getThumbnailBaseUrl() . rawurlencode($thumbfilename);
    }



    /**
     * Get a thumbnail url without building it
     * the first display of the image will be slower but each request to get the url of a thumbnail will be quicker
     * this method is not compatible with the <code>setSourceBinary</code> method
     *
     * @param	int				$width
     * @param	int				$height
     *
     * @return string | null 		the thumb file name or null if the ressource of original file cannot be found.
     */
    public function getDelayedThumbnail($width, $height) {
        require_once dirname(__FILE__).'/thumbnail.php';

        $obj = new lfm_callThumbnail;

        $url = $obj->getUrl($this->filePath, $width, $height, $this->effect_options);

        if (null === $url) {
            // bab_debug(sprintf('cannot open original image file %s', $this->filePath), DBG_WARNING, 'thumbnailer');
            return null;
        }

        return $url;
    }


    /**
     * Get the thumbnail url only if allready created
     * return null if the file has been modified since the last thumbnail
     *
     * @param	int				$width
     * @param	int				$height
     *
     * @return string
     */
    public function getCreatedThumbnail($width, $height) {
        require_once dirname(__FILE__).'/thumbnail.php';
        return lfm_getUpToDateThumbUrl($this->filePath, $width, $height, $this->effect_options);
    }



    /**
     * Get the resized image
     * @param $width
     * @param $height
     * @return bab_Path | null | false		the thumb file path
     */
    public function getThumbnailPath($width, $height) {
        require_once dirname(__FILE__).'/thumbnail.php';
        require_once $GLOBALS['babInstallPath'].'utilit/path.class.php';

        $thumbfilename = lfm_getThumbnail($this->filePath, $this->fileData, $width, $height, $this->effect_options);

        if (false === $thumbfilename) {
            bab_debug(sprintf('cannot write thumbnail image file in %s', lfm_getThumbnailBasePath()), DBG_WARNING, 'thumbnailer');
            return false;
        }

        if (null === $thumbfilename) {
            bab_debug(sprintf('cannot open original image file %s', $this->filePath), DBG_WARNING, 'thumbnailer');
            return null;
        }

        return new bab_Path(lfm_getThumbnailBasePath() . $thumbfilename);
    }



    /**
     * Get thumbnail image as a base64 string for inline html source
     *
     *
     * @since 0.2.28
     *
     * @param unknown_type $width
     * @param unknown_type $height
     *
     * @return string
     */
    public function getThumbnailInline($width, $height) {
        $path = $this->getThumbnailPath($width, $height);
        if ($path instanceof bab_Path) {

            $mime = bab_getFileMimeType($path->tostring());

            return 'data:'.$mime.';base64,'.base64_encode(file_get_contents($path->tostring()));
        }

        return null;
    }




    /**
     * Get The resized image or a no photo image if source file not available
     *
     * @param	int				$width
     * @param	int				$height
     * @return 	string			a path to image
     */
    function getThumbnailOrDefault($width, $height) {
        $src = $this->getThumbnail($width, $height);

        if (false === $src || null === $src) {
            $this->filePath = $GLOBALS['babInstallPath'].'skins/ovidentia/images/nophoto.jpg';
            return $this->getThumbnail($width, $height);
        }

        return $src;
    }

    /**
     * Get Icon or thumbnail if possible
     * @since	0.2.1
     *
     * @param	string	$filepath	source file
     * @param	int		$width		thumbnail max width
     * @param	int		$height		thumbnail max height
     * @return	lfm_FileIcon
     */
    function getIcon($filepath, $width, $height) {
        require_once dirname(__FILE__).'/thumbnail.php';
        $icon = new lfm_FileIcon($filepath, $width, $height);
        $icon->setEffects($this->effect_options);

        return $icon;
    }





    /**
     * Get generic classname for a file based on freedesktop recomandations
     * A categorisation of file to implement a generic icons set
     *
     *  # mimetype-audio-x-generic
     *	# mimetype-image-x-generic
     *	# mimetype-package-x-generic
     * 	# mimetype-text-x-generic
     *	# mimetype-video-x-generic
     *	# mimetype-x-office-document
     *	# mimetype-x-office-presentation
     *	# mimetype-x-office-spreadsheet
     *	# mimetype-unknown
     *
     * @since	0.2.1
     *
     * @see 	http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
     * @param	string	$filepath
     * @return 	string
     */
    function getGenericClassName($filepath = null) {

        if (null === $filepath)
        {
            $filepath = $this->filePath;
        }

        $mime = bab_getFileMimeType($filepath);
        require_once dirname(__FILE__).'/classname.php';
        return lfm_getGenericClassName($mime);
    }
}










/**
 * File infos
 * Get file type and metadatas
 */
class Func_FileInfos extends bab_functionality {

    /**
     * Get description of functionality
     * @return string
     */
    function getDescription() {
        return lfm_translate('Get informations on a file, mime type, metadata, etc ...');
    }

    /**
     * Try to gess mime type with fileinfo extension or file command
     * if nothing is available, this method fallback to a test on file extension
     * if there is no reading permissions, the method return false
     *
     * @param	string	$filepath
     * @return	string|false
     */
    function getMimeTypeFromFile($filepath) {

        if (!is_readable($filepath)) {
            return false;
        }


        /*
        if (function_exists('mime_content_type')) {
            $mime = mime_content_type($filepath);
            if ($mime !== false) {
                return $mime;
            }
        }
        */

        if (function_exists('finfo_open')) {
            $finfo = finfo_open(FILEINFO_MIME);
            $mime = finfo_file($finfo, $filepath);
            finfo_close($finfo);
            if ($mime !== false) {

                if (empty($mime)) {
                    return 'application/octet-stream';
                }
                return $mime;
            }
        }


        $cmd = 'file --mime -b '.escapeshellarg($filepath).' 2>/dev/null';
        ob_start();
        passthru($cmd, $exit_code);
        $content = ob_get_contents();
        ob_end_clean();
        $content = trim($content);

        if (0 === $exit_code && 'data' !== $content  && '' !== $content) {
            // check for multiple mime types

            $tmparr = explode(' ', $content);
            if (1 === count($tmparr)) {
                return $content;
            }
            // get first ...
            return $tmparr[0];
        }

        return $this->getMimeTypeFromExtension($filepath);
    }


    /**
     * Get mime type from extension
     * @param	string	$filename
     * @return	string
     */
    function getMimeTypeFromExtension($filename) {
        return bab_getFileMimeType($filename);
    }

    /**
     * Get a file type description from a mime type
     * This method implement a selection of translated descriptions from the freedesktop project
     * @param	string	$mime
     * @return 	string
     */
    function getFileTypeFromMimeType($mime) {
        $str = lfm_translate($mime);
        if ($str !== $mime) {
            return $str;
        }
        return lfm_translate('Unrecognized file type');
    }


    /**
     * Get metadata based on freedesktop recomandations
     * The method return a lfm_MetaNamespaces object. metadata are accessible into this object
     *
     * ex : for 'Doc.Subject'
     *		$meta->Doc->Subject
     *
     * If meta does not exists, a lfm_MetadataException will be fired
     *
     * @see lfm_MetadataException
     * @see lfm_MetaNamespaces
     *
     * @link http://freedesktop.org/wiki/Specifications/shared-filemetadata-spec
     * @param	string	$filepath
     *
     * @return lfm_MetaNamespaces | false		false is returned only if there is a reading error
     */
    function getMetadata($filepath) {
        require_once dirname(__FILE__).'/metadata.php';
        return lfm_getMetadata($filepath, $this->getMimeTypeFromExtension($filepath));
    }


    /**
     * Get generic classname for a file based on freedesktop recomandations
     * A categorisation of file to implement a generic icons set
     *
     *  # mimetype-audio-x-generic
     *	# mimetype-image-x-generic
     *	# mimetype-package-x-generic
     * 	# mimetype-text-x-generic
     *	# mimetype-video-x-generic
     *	# mimetype-x-office-document
     *	# mimetype-x-office-presentation
     *	# mimetype-x-office-spreadsheet
     *	# mimetype-unknown
     *
     * @see http://standards.freedesktop.org/icon-naming-spec/icon-naming-spec-latest.html
     * @param	string	$filepath
     * @return 	string
     */
    function getGenericClassName($filepath) {
        $mime = $this->getMimeTypeFromExtension($filepath);
        require_once dirname(__FILE__).'/classname.php';
        return lfm_getGenericClassName($mime);
    }

}








/**
 * Functionality for directories
 */
class Func_DirectoryManager extends bab_functionality {


    /**
     * Get description of functionality
     * @return string
     */
    function getDescription() {
        return lfm_translate('Delete and create folders');
    }


    /**
     * Delete directory recursively
     * @param	string		$dir
     * @return 	boolean
     */
    function deleteRecursively($dir) {

        $dir = rtrim($dir, '/ ');

        if (!is_dir($dir)) {
            return false;
        }

        $current_dir = opendir($dir);
        while(false !== ($entryname = readdir($current_dir))) {
            $subdir = $dir.'/'.$entryname;

            if (is_dir($subdir) && ($entryname !== '.' && $entryname !== '..')) {
                if (false === $this->deleteRecursively($subdir)) {
                    return false;
                }
            } elseif ($entryname !== "." && $entryname !== '..') {
                if (false === unlink($subdir)) {
                    return false;
                }
            }
        }

        closedir($current_dir);
        rmdir($dir);

        return true;
    }







    /**
     * Create a directory if not exists
      * @param	string	$str	: directory full path
      * @return	boolean
      */
    function create($str) {
        $path = explode('/',$str);
        $created = '';
        foreach($path as $d) {
            $created.= '/'.$d;
            if (!is_dir($created)) {
                if (!bab_mkdir($created)) {
                    return false;
                }
            }
        }

        return true;
    }

}