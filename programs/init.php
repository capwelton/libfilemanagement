<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

function LibFileManagement_upgrade($version_base, $version_ini)
{
    $addon = bab_getAddonInfosInstance('LibFileManagement');

    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();

    // the upload fonctionality is deprecated in this addon widget file picker must be used instead
    $functionalities->unregister('FileUploader/ImageUploader');
    $functionalities->unregister('FileUploader/GenericUploader');
    $functionalities->unregister('FileUploader');

    $functionalities->register('Thumbnailer', $addon->getPhpPath() . 'functionalities.php');
    $functionalities->register('FileInfos', $addon->getPhpPath() . 'functionalities.php');
    $functionalities->register('DirectoryManager', $addon->getPhpPath() . 'functionalities.php');
    $functionalities->register('ExcelExport', $addon->getPhpPath() . 'excel.class.php');
    $functionalities->register('ExcelxExport', $addon->getPhpPath() . 'excelx.class.php');
    $functionalities->register('FileListUploader', $addon->getPhpPath() . 'multiplefile.php');
    $functionalities->register('Ovml/Function/Thumbnail', $addon->getPhpPath().'thumbnail_ovml.php');
    $functionalities->register('Ovml/Function/ImageCrop', $addon->getPhpPath().'imagecrop_ovml.php');
    $functionalities->register('Ovml/Function/Img', $addon->getPhpPath().'img_ovml.php');

    return true;
}



function LibFileManagement_onDeleteAddon() {

    require_once $GLOBALS['babInstallPath'].'utilit/functionalityincl.php';
    $functionalities = new bab_functionalities();

    if (false === $functionalities->unregister('FileUploader/ImageUploader')) {
        return false;
    }

    if (false === $functionalities->unregister('FileUploader/GenericUploader')) {
        return false;
    }

    if (false === $functionalities->unregister('FileUploader')) {
        return false;
    }

    if (false === $functionalities->unregister('Thumbnailer')) {
        return false;
    }

    if (false === $functionalities->unregister('FileInfos')) {
        return false;
    }

    if (false === $functionalities->unregister('DirectoryManager')) {
        return false;
    }

    if (false === $functionalities->unregister('ExcelExport')) {
        return false;
    }

    if (false === $functionalities->unregister('FileListUploader')) {
        return false;
    }

    $functionalities->unregister('Ovml/Function/Thumbnail');
    $functionalities->unregister('Ovml/Function/ImageCrop');
    $functionalities->unregister('Ovml/Function/Img');

    return true;
}