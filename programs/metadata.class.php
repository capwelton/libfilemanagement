<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";
require_once dirname(__FILE__)."/functions.php";



class lfm_MetadataException extends Exception {

}






/**
 * Return value for a metadata request
 * propose access to multiple meta namespaces
 */
class lfm_MetaNamespaces {

	private $filepath = null;
	private $namespaces = array();

	public function addNamespace($namespace, $classname) {
		$this->namespaces[$namespace] = $classname;
	}

	public function setFilePath($filepath) {
		$this->filepath = $filepath;
		return $this;
	}

	public function getFilePath() {
		return $this->filepath;
	}

	public function __get($name) {
		if (!isset($this->namespaces[$name])) {
			throw new lfm_MetadataException(sprintf('This metadata namespace name is not allowed : %s', $name));
		}

		if (is_string($this->namespaces[$name])) {
			$classname = $this->namespaces[$name];
			$this->namespaces[$name] = new $classname();
			$this->namespaces[$name]->setMetaNamespaces($this);
		}

		return $this->namespaces[$name];
	}

	public function __isset($name) {
		if (!isset($this->namespaces[$name])) {
			return false;
		}

		return true;
	}

	public function getAllNs() {
		return array_keys($this->namespaces);
	}
}





/**
 * Metadata collection
 * list of metadata associated to a metadata namespace
 */
abstract class lfm_MetadataCollection {


	protected $name;
	protected $description;

	private $nspaces 		= null;
	private $meta 			= array();

	/**
	 * set file path
	 * @param	string
	 * @return lfm_MetadataCollection
	 */
	public function setMetaNamespaces(lfm_MetaNamespaces $nspaces) {
		$this->nspaces = $nspaces;
		return $this;
	}

	public function getFilePath() {
		return $this->nspaces->getFilePath();
	}


	final public function getNamespaceName() {
		return $this->name;
	}

	final public function getNamespaceDescription() {
		return $this->description;
	}


	final public function __isset($name) {
		if ($this->isMeta($name)) {
			return true;
		}

		return false;
	}

	protected function getStoredValue($name) {
		if (!isset($this->meta[$name])) {
			return null;
		}

		return $this->meta[$name];
	}

	final public function __get($name) {
		if (false === $this->isMeta($name)) {
			throw new lfm_MetadataException(sprintf('This metadata does not exists : %s', $this->name.'.'.$name));
		}

		return $this->getDisplayValue($name);
	}


	/**
	 * Store metadata informations
	 * @return lfm_MetadataCollection
	 */
	final protected function setMeta($name, $value) {
		$this->meta[$name] = $value;
		return $this;
	}

	/**
	 * Test if a metadata exists
	 * this method should return true if the metadata can be obtained
	 * @param	string	$name
	 * @return bool
	 */
	final protected function isMeta($name) {
		$arr = $this->getAllMeta();
		return in_array($name, $arr);
	}

	/**
	 * @return array
	 */
	abstract public function getAllMeta();


	/**
	 * Get a value for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	abstract public function getMetaValue($name);


	/**
	 * Get a displayable human readable version of the metadata value
	 * @param string $name
	 * @return string
	 */
	public function getDisplayValue($name)
	{
		if (isset($this->meta[$name]))
		{
			$value = $this->meta[$name];
		} else
		{
			$value = $this->getMetaValue($name);
			$this->setMeta($name, $value);
		}


		return (string) $value;
	}


	/**
	 * Get a title for a metadata name
	 * @param	string	$name
	 * @return mixed
	 */
	abstract public function getTitle($name);
}






/**
 * File namespace
 * @link http://freedesktop.org/wiki/Specifications/shared-filemetadata-spec
 */
abstract class lfm_MetadataNamespace_File extends lfm_MetadataCollection {
	public function __construct() {
		$this->name 		= 'File';
		$this->description 	= lfm_translate('File');
	}


	public function getTitle($name) {
		switch($name) {
			case 'Name': 				return lfm_translate('File name');
			case 'Link':				return lfm_translate('Link');
			case 'Format':				return lfm_translate('Mime type');
			case 'Size':				return lfm_translate('Size');
			case 'Keywords': 			return lfm_translate('Document keywords');
			case 'Description': 		return lfm_translate('Description');
			case 'Publisher': 			return lfm_translate('Publisher name');
			case 'Modified': 			return lfm_translate('Last modified datetime');
			case 'Created': 			return lfm_translate('Creation datetime');
			case 'Accessed': 			return lfm_translate('Last access datetime');
			case 'IconPath': 			return lfm_translate('Icon');
			case 'SmallThumbnailPath': 	return lfm_translate('Small thumbnail');
			case 'LargeThumbnailPath': 	return lfm_translate('Large thumbnail');
			case 'Rank': 				return lfm_translate('Rank');

			default: 			throw new lfm_MetadataException(sprintf('Unexpected metadata name File.%s', $name));
		}
	}
}


/**
 * Audio namespace
 * @link http://freedesktop.org/wiki/Specifications/shared-filemetadata-spec
 */
abstract class lfm_MetadataNamespace_Audio extends lfm_MetadataCollection {
	public function __construct() {
		$this->name 		= 'Audio';
		$this->description 	= lfm_translate('Audio');
	}



	public function getTitle($name) {
		switch($name) {
			case 'Title': 		return lfm_translate('Title');
			case 'Artist': 		return lfm_translate('Artist');
			case 'Album': 		return lfm_translate('Album');
			case 'Comment': 	return lfm_translate('Comment');
			case 'TrackNo': 	return lfm_translate('Track number');
			case 'Genre': 		return lfm_translate('Genre');
			case 'Samplerate': 	return lfm_translate('Audio samplerate in Hz');
			case 'Bitrate': 	return lfm_translate('Audio bitrate in kbps');
			case 'Codec': 		return lfm_translate('Audio codec');

			default: 			throw new lfm_MetadataException(sprintf('Unexpected metadata name Audio.%s', $name));
		}
	}
}


/**
 * Document namespace
 * @link http://freedesktop.org/wiki/Specifications/shared-filemetadata-spec
 */
abstract class lfm_MetadataNamespace_Doc extends lfm_MetadataCollection {
	public function __construct() {
		$this->name 		= 'Doc';
		$this->description 	= lfm_translate('Document');
	}


	public function getTitle($name) {
		switch($name) {
			case 'Title': 		return lfm_translate('Title of document');
			case 'Subject': 	return lfm_translate('Document subject');
			case 'Author': 		return lfm_translate('Name of the author');
			case 'Keywords': 	return lfm_translate('Document keywords');
			case 'Comments': 	return lfm_translate('Comments');
			case 'PageCount': 	return lfm_translate('Number of pages in document');
			case 'WordCount': 	return lfm_translate('Number of words in document');
			case 'Created': 	return lfm_translate('Creation datetime');

			default: 			throw new lfm_MetadataException(sprintf('Unexpected metadata name Doc.%s', $name));
		}
	}
}



/**
 * Image namespace
 * @link http://freedesktop.org/wiki/Specifications/shared-filemetadata-spec
 */
abstract class lfm_MetadataNamespace_Image extends lfm_MetadataCollection {
	public function __construct() {
		$this->name 		= 'Image';
		$this->description 	= lfm_translate('Image');
	}





	public function getTitle($name) {
		switch($name) {
			case 'Height': 			return lfm_translate('Height');
			case 'Width': 			return lfm_translate('Width');
			case 'Title': 			return lfm_translate('Title');
			case 'Date': 			return lfm_translate('Date');
			case 'Keywords': 		return lfm_translate('Keywords');
			case 'Comments': 		return lfm_translate('Comments');
			case 'Description': 	return lfm_translate('Description');
			case 'Software': 		return lfm_translate('Software');
			case 'Orientation': 	return lfm_translate('Orientation');
			case 'CameraMake': 		return lfm_translate('Camera make');
			case 'CameraModel': 	return lfm_translate('Camera model');
			case 'GPSLatitude': 	return lfm_translate('Latitude');
			case 'GPSLongitude': 	return lfm_translate('Longitude');
			case 'GPSAltitude': 	return lfm_translate('Altitude in meter');
            case 'Copyright':       return lfm_translate('Copyright');
            case 'Author':          return lfm_translate('Author');

			default: 			throw new lfm_MetadataException(sprintf('Unexpected metadata name Image.%s', $name));
		}
	}
}




