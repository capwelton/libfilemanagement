<?php
/************************************************************************
 * OVIDENTIA http://www.ovidentia.org                                   *
 ************************************************************************
 * Copyright (c) 2003 by CANTICO ( http://www.cantico.fr )              *
 *                                                                      *
 * This file is part of Ovidentia.                                      *
 *                                                                      *
 * Ovidentia is free software; you can redistribute it and/or modify    *
 * it under the terms of the GNU General Public License as published by *
 * the Free Software Foundation; either version 2, or (at your option)  *
 * any later version.													*
 *																		*
 * This program is distributed in the hope that it will be useful, but  *
 * WITHOUT ANY WARRANTY; without even the implied warranty of			*
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.					*
 * See the  GNU General Public License for more details.				*
 *																		*
 * You should have received a copy of the GNU General Public License	*
 * along with this program; if not, write to the Free Software			*
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,*
 * USA.																	*
************************************************************************/
require_once "base.php";

require_once dirname(__FILE__).'/metadata.class.php';




class lfm_ExifMetadata extends lfm_MetadataNamespace_Image {

    private $size = null;
    private $exif = null;
    private $iptc = null;


    public function getAllMeta() {

        $arr = array(
            'Height',
            'Width',
            'Description',
            'Keywords'
        );

        if (function_exists('exif_read_data')) {

            $arr = array_merge($arr, array(
                'Date',
                'Orientation',
                'Software',
                'Comments',
                'CameraMake',
                'CameraModel',
                'GPSLatitude',
                'GPSLongitude',
                'GPSAltitude',
                'Copyright',
                'Author'
            ));

        }

        return $arr;
    }


    /**
     * Get a value for a metadata name
     * @param	string	$name
     * @return mixed
     */
    public function getMetaValue($name) {

        switch($name) {

            case 'Width':
            case 'Height':
                return $this->getSize($name);

            case 'Description':
            case 'Keywords':
                return $this->getIptc($name);

            case 'Date':
            case 'Orientation':
            case 'Software':
            case 'Comments':
            case 'CameraMake':
            case 'CameraModel':
            case 'GPSLatitude':
            case 'GPSLongitude':
            case 'GPSAltitude':
            case 'Copyright':
            case 'Author':
                return $this->getExif($name);

            default:
                return '';

        }

        return $this->getStoredValue($name);
    }


    /**
     * @return string
     */
    public function getDisplayValue($name)
    {
        $value = $this->getMetaValue($name);
        if (null === $value)
        {
            return null;
        }


        switch($name)
        {
            case 'Width':
            case 'Height':
                return $value.' px';

            case 'Date':
                return bab_shortDate($value);

            case 'Orientation':
                if (1 === $value) {
                    return lfm_translate('top,left');
                } else {
                    return lfm_translate('bottom,right');
                }


            case 'GPSLatitude':
            case 'GPSLongitude':
                return sprintf('%s', number_format($value,2));

        }

        return $this->getMetaValue($name);
    }




    private function getSize($name) {
        if (null === $this->size) {
            $this->size = getimagesize($this->getFilePath());

        }

        switch($name) {
            case 'Width':	return $this->size[0];
            case 'Height': 	return $this->size[1];
        }
    }




    private function getIptc($name) {
        if (null === $this->iptc) {
            $this->iptc = $this->get_iptcData();
        }


        switch($name) {
            case 'Description':
                if (isset($this->iptc['2#120'][0])) {
                    $description = bab_getStringAccordingToDatabase($this->iptc['2#120'][0], 'ISO-8859-15');
                    return $description;
                }
                break;

            case 'Keywords':
                if (isset($this->iptc['2#025'])) {
                    return bab_getStringAccordingToDatabase(implode(', ', $this->iptc['2#025']), 'ISO-8859-15');
                }
                break;

        }

        return null;
    }

    /**
     * Get IPTC
     * @return array
     */
    private function get_iptcData() {

        $return = array();

        $size = getimagesize ( $this->getFilePath(), $info);
        if(is_array($info) && isset($info["APP13"])) {
            $return = iptcparse($info["APP13"]);
        }
        return $return;
    }




    private function getExif($name) {

        if (!function_exists('exif_read_data')) {
            return null;
        }


        if (null === $this->exif) {
            $this->exif = @exif_read_data($this->getFilePath());
        }

        switch($name) {
            case 'Date':
                $date = $this->getExifKey('DateTime');

                if (empty($date)) {
                    return null;
                }

                if (preg_match('/([0-9]{4}).([0-9]{2}).([0-9]{2})\s([0-9]{2}):([0-9]{2}):([0-9]{2})/', $date, $m)) {
                    $date = mktime($m[4], $m[5], $m[6], $m[2], $m[3], $m[1]);
                    return $date;
                }

                return null;

            case 'CameraMake':	return $this->getExifKey('Make');
            case 'CameraModel':	return $this->getExifKey('Model');
            case 'Orientation':
                $orientation = $this->getExifKey('Orientation');

                if (!$orientation) {
                    return null;
                }
                return $orientation;


            case 'Software':
                return $this->getExifKey('Software');

            case 'Copyright':
                return $this->getExifKey('Copyright');

            case 'Author':
                return $this->getExifKey('Author');

            case 'Comments':
                $computed = $this->getExifKey('COMPUTED');
                if (!isset($computed)) {
                    return null;
                }

                $comment = isset($computed['UserComment']) ? $computed['UserComment'] : null;

                if (empty($comment)) {
                    return null;
                }

                $charset = isset($computed['UserCommentEncoding']) ? $computed['UserCommentEncoding'] : null;

                switch($charset) {

                    case 'UNICODE':
                        $charset = 'UTF-8';
                        break;

                    case 'UNDEFINED':
                    default:
                        $charset = 'ISO-8859-1';
                }

                return bab_getStringAccordingToDataBase($comment, $charset);

            case 'GPSLatitude':
                $latitude = $this->getExifKey('GPSLatitude');

                if (!isset($latitude)) {
                    return null;
                }

                $ref = $this->getExifKey('GPSLatitudeRef');			// North or south latitude
                $latitude = $this->llconv($latitude);

                if ('S' === $ref) {
                    $latitude *= -1;
                }

                return $latitude;

            case 'GPSLongitude':
                $longitude = $this->getExifKey('GPSLongitude');

                if (!isset($longitude)) {
                    return null;
                }

                $ref = $this->getExifKey('GPSLongitudeRef');		// East or west longitude
                $longitude = $this->llconv($longitude);

                if ('W' === $ref) {
                    $longitude *= -1;
                }

                return $longitude;

            case 'GPSAltitude':
                $altitude = $this->getExifKey('GPSAltitude');
                if ($f = $this->divstring($altitude)) {
                    return round($f);
                }

                return $altitude;
        }
    }

    /**
     * convert latitude or longitude
     * @return float
     */
    private function llconv($arr) {

        if (!is_array($arr)) {
            return 0.0;
        }

        if (3 !== count($arr)) {
            return 0.0;
        }

        $deg = $this->divstring($arr[0]);
        $min = $this->divstring($arr[1]);
        $sec = $this->divstring($arr[2]);

        if (false === $deg || false === $min || false === $sec) {
            return 0.0;
        }


        $deg += ($min + ($sec / 60)) / 60;

        return $deg;
    }


    /**
     * calcul of a string xx/yy
     * @return float | int
     */
    private function divstring($str) {
        $arr = explode('/', $str);
        if (isset($arr[0]) && isset($arr[1])) {
            return $arr[0] / $arr[1];
        }

        return false;
    }



    /**
     * @param	string	$key
     * @return string
     */
    private function getExifKey($key) {

        $return = null;

        if (false !== $this->exif) {
            if (isset($this->exif[$key])) {
                $return = $this->exif[$key];
            }
        }

        return $return;
    }
}





