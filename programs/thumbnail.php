<?php
//-------------------------------------------------------------------------
// OVIDENTIA http://www.ovidentia.org
// Ovidentia is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2, or (at your option)
// any later version.
//
// This program is distributed in the hope that it will be useful, but
// WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
// See the GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA 02111-1307,
// USA.
//-------------------------------------------------------------------------
/**
 * @license http://opensource.org/licenses/gpl-license.php GNU General Public License (GPL)
 * @copyright Copyright (c) 2006 by CANTICO ({@link http://www.cantico.fr})
 */
include_once 'base.php';
include_once dirname(__FILE__).'/functions.php';

/**
 * The biggest size (width * height) for an image
 * for which thumbnail created will be attempted.
 *
 * @var int
 */
define('LFM_BIGGEST_THUMBNAILABLE_GD_IMAGE_SURFACE', 24000000);

/**
 * Returns the physical path to the thumbnail folder.
 *
 * @return string
 */
function lfm_getThumbnailBasePath()
{
    $addon = bab_getAddonInfosInstance('LibFileManagement');

    if (!$addon) {
        return '';
    }

    $ovidentiapath = realpath('.');
    return $ovidentiapath.'/images/'.$addon->getRelativePath().'thumbnails/';

}


/**
 * Returns the base url to access a thumbnail picture.
 *
 * @return string
 */
function lfm_getThumbnailBaseUrl()
{

    $addon = bab_getAddonInfosInstance('LibFileManagement');

    if (!$addon) {
        return '';
    }

    return 'images/'.$addon->getRelativePath().'thumbnails/';

}


/**
 * Determines which image file format to use depending on the thumbnail size.
 *
 * @param int $width  The thumbnail width in pixels.
 * @param int $height The thumbnail height in pixels.
 *
 * @return string The image format 'png' or 'jpeg'.
 */
function lfm_getThumbnailBestImageFormat($width, $height)
{
    if ($width * $height > 10000) {
        return 'jpeg';
    }
    return 'png';
}

/**
 * Returns the thumbnail filename for a given file and size.
 *
 * @param string			$basestring			Initial string to perform crc
 * @param int				$thumbWidth			The thumbnail width.
 * @param int				$thumbHeight		The thumbnail height.
 * @param	false|array		$effect_options		array of options for associated effect
 * @return string
 */
function lfm_getThumbFilename($basestring, $thumbWidth, $thumbHeight, $effect_options, $lastupdate)
{
    if (!isset($effect_options['imageformat']) || empty($effect_options['imageformat']) || $effect_options['imageformat'] === 'auto') {
        $effect_options['imageformat'] = lfm_getThumbnailBestImageFormat($thumbWidth, $thumbHeight);
    }

    $ustr = $basestring;

    if (false !== $effect_options) {
        $ustr .= serialize($effect_options);
    }

    return sprintf(',%u-%s-%dx%d' . '.' . $effect_options['imageformat'], crc32($ustr), $lastupdate, $thumbWidth, $thumbHeight);
}



/**
 * Checks whether the specified file has a valid thumbnail.
 *
 * @param 	string	$originalTimestamp	last update on original file
 * @param	string	$thumbFilename
 * @return	boolean
 */
function lfm_thumbnailIsUpTodate($originalTimestamp,  $thumbFilename)
{
    $thumbPath = lfm_getThumbnailBasePath();

    $filePath = $thumbPath . $thumbFilename;

    if (!file_exists($filePath)) {
        return false;
    }

    if (!is_readable($filePath)) {
        return false;
    }

    if ($originalTimestamp < filemtime($filePath)) {
        return true;
    }
    return false;
}

if (!function_exists('imagecreatefrombmp')) {
    // The function is native from PHP >= 7.2.0
    function imagecreatefrombmp($p_sFile)
    {
        $file    =    fopen($p_sFile,"rb");
        $read    =    fread($file,10);
        while(!feof($file)&&($read<>""))
            $read    .=    fread($file,1024);
        $temp    =    unpack("H*",$read);
        $hex    =    $temp[1];
        $header    =    substr($hex,0,108);
        if (substr($header,0,4)=="424d")
        {
            $header_parts    =    str_split($header,2);
            $width            =    hexdec($header_parts[19].$header_parts[18]);
            $height            =    hexdec($header_parts[23].$header_parts[22]);
            unset($header_parts);
        }
        $x                =    0;
        $y                =    1;
        $image            =    imagecreatetruecolor($width,$height);
        $body            =    substr($hex,108);
        $body_size        =    (strlen($body)/2);
        $header_size    =    ($width*$height);
        $usePadding        =    ($body_size>($header_size*3)+4);
        for ($i=0;$i<$body_size;$i+=3)
        {
            if ($x>=$width)
            {
                if ($usePadding)
                    $i    +=    $width%4;
                $x    =    0;
                $y++;
                if ($y>$height)
                    break;
            }
            $i_pos    =    $i*2;
            $r        =    hexdec($body[$i_pos+4].$body[$i_pos+5]);
            $g        =    hexdec($body[$i_pos+2].$body[$i_pos+3]);
            $b        =    hexdec($body[$i_pos].$body[$i_pos+1]);
            $color    =    imagecolorallocate($image,$r,$g,$b);
            imagesetpixel($image,$x,$height-$y,$color);
            $x++;
        }
        unset($body);
        return $image;
    }
}


/**
 * create image ressource from file and imageinfos
 * @return resource|false
 */
function lfm_createGdImageFromFilename($filename, $imageInfo)
{
    list($width, $height) = $imageInfo;
    if ($width * $height > LFM_BIGGEST_THUMBNAILABLE_GD_IMAGE_SURFACE) {
        bab_debug('surface is bigger than LFM_BIGGEST_THUMBNAILABLE_GD_IMAGE_SURFACE, no thumbnail for '.$filename);
        return null;
    }
    $image = null;
    switch ($imageInfo['mime']) {
        case 'image/gif':
            if (imagetypes() & IMG_GIF) {
                $image = imageCreateFromGif($filename);
            }
            break;
        case 'image/jpeg':
            if (imagetypes() & IMG_JPG)  {
                $image = imageCreateFromJpeg($filename);
            }
            break;
        case 'image/png':
            if (imagetypes() & IMG_PNG)  {
                $image = imageCreateFromPng($filename);
                // imagealphablending($image, false);

            }
            break;
		case 'image/x-ms-bmp':
			$image = imagecreatefrombmp($filename);
            break;
        case 'image/wbmp':
            if (imagetypes() & IMG_WBMP)  {
                $image = imageCreateFromWbmp($filename);
            }
            break;
		default:
			bab_debug('Unkown GD image type: '.$imageInfo['mime']);
    }


    $fileInfos = bab_functionality::get('FileInfos');
    $meta = $fileInfos->getMetadata($filename);

    if (isset($meta->Image)) {
        if ($orientation = $meta->Image->getMetaValue('Orientation')) {
            switch($orientation) {
                case 1: // nothing
                break;

                case 2: // horizontal flip
                    //$image->flipImage($public,1);
                break;

                case 3: // 180 rotate left
                    $image = imagerotate($image, 180, 0);
                break;

                case 4: // vertical flip
                    //$image->flipImage($public,2);
                break;

                case 5: // vertical flip + 90 rotate right
                   // $image->flipImage($public, 2);
                    $image = imagerotate($image, -90, 0);
                break;

                case 6: // 90 rotate right
                    $image = imagerotate($image, -90, 0);
                break;

                case 7: // horizontal flip + 90 rotate right
                    //$image->flipImage($public,1);
                    $image = imagerotate($image, -90, 0);
                break;

                case 8: // 90 rotate left
                    $image = imagerotate($image, 90, 0);
                break;
            }
        }
    }

    return $image;
}






/**
 * create GD thumbnail from GD Image
 *
 * @param	ressource	$image
 * @param	int			$thumbWidth
 * @param	int			$thumbHeight
 * @param 	string		$thumbFilename		Thumb file name
 * @param	array		$effect_options
 *
 * @return ressource	thumbnail GD Image
 */
function lfm_createGdThumbnailFromGdImage($image, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options)
{
    //CROP AREA TREATMENT
    if (isset($effect_options['croparea'])) {
        if(isset($effect_options['croparea']['rotate']) && $effect_options['croparea']['rotate']) {
            imagealphablending($image, false);
            imagesavealpha($image, true);
            $image = imagerotate($image, $effect_options['croparea']['rotate'], imagecolorallocatealpha($image, 0, 0, 0, 127));
        }
        $image2 = imagecreatetruecolor($effect_options['croparea']['width'], $effect_options['croparea']['height']);
        imagealphablending($image2, false);
        imagesavealpha($image2, true);
		imagecolorallocatealpha($image2, 0, 0, 0, 127);

		imagealphablending($image, false);
        imagesavealpha($image, true);
        imagecopy(
            $image2, $image,
            0, 0,
            $effect_options['croparea']['x'],
            $effect_options['croparea']['y'],
            $effect_options['croparea']['width'],
            $effect_options['croparea']['height']
        );
        $image = $image2;
    }

    $width = imagesx($image);
    $height = imagesy($image);

    if (!isset($effect_options['resizemode'])) {
        $resizemode = Func_Thumbnailer::KEEP_ASPECT_RATIO;
    } else {
        $resizemode = $effect_options['resizemode'];
    }

    switch ($resizemode) {

        case Func_Thumbnailer::CROP_CENTER:
            $resizeleft = isset($effect_options['resizeleft']) ? $effect_options['resizeleft'] : 0.5;
            if ($resizeleft < 0) {
                $resizeleft = 0;
            } elseif ($resizeleft > 1) {
                $resizeleft = 1;
            }
            $resizetop = isset($effect_options['resizetop']) ? $effect_options['resizetop'] : 0.5;
            if ($resizetop < 0) {
                $resizetop = 0;
            } elseif ($resizetop > 1) {
                $resizetop = 1;
            }
            if ($height > $thumbHeight && $width > $thumbWidth) {
                $widthRatio = $thumbHeight / $height;
                $heightRatio = $thumbWidth / $width;

                if ($heightRatio > $widthRatio) {
                    $firstThumbWidth = round($width * $heightRatio);
                    $firstThumbHeight = round($height * $heightRatio);
                } else {
                    $firstThumbWidth = round($width * $widthRatio);
                    $firstThumbHeight = round($height * $widthRatio);
                }
            } else {
                $firstThumbWidth = $width;
                $firstThumbHeight = $height;
            }

            if ($thumbWidth > $firstThumbWidth) {
                $thumbWidth = $firstThumbWidth;
            }

            if ($thumbHeight > $firstThumbHeight) {
                $thumbHeight = $firstThumbHeight;
            }

            $thumbImage = imageCreateTrueColor($firstThumbWidth, $firstThumbHeight);
            imagealphablending($thumbImage, false);

            imageCopyResampled($thumbImage, $image, 0, 0, 0, 0, $firstThumbWidth, $firstThumbHeight, $width, $height);

            imageDestroy($image);

            $thumbImageCropped = imageCreateTrueColor($thumbWidth, $thumbHeight);
            imagealphablending($thumbImageCropped, false);
            imagecopy($thumbImageCropped , $thumbImage , 0, 0 , ($firstThumbWidth - $thumbWidth) * $resizeleft, ($firstThumbHeight - $thumbHeight) * $resizetop, $thumbWidth, $thumbHeight);

            $thumbImage = $thumbImageCropped;
            break;

        case Func_Thumbnailer::KEEP_ASPECT_RATIO:
        default:
            if ($height > $thumbHeight || $width > $thumbWidth) {
                $widthRatio = $thumbHeight / $height;
                $heightRatio = $thumbWidth / $width;

                $firstThumbWidth = $thumbWidth;
                $firstThumbHeight = $thumbHeight;

                if ($heightRatio > $widthRatio) {
                    $firstThumbWidth = round($width * $widthRatio);
                } else {
                    $firstThumbHeight = round($height * $heightRatio);
                }
            } else {
                $firstThumbWidth = $width;
                $firstThumbHeight = $height;
            }

            $thumbImage = imageCreateTrueColor($firstThumbWidth, $firstThumbHeight);
            imagealphablending($thumbImage, false);

            imageCopyResampled($thumbImage, $image, 0, 0, 0, 0, $firstThumbWidth, $firstThumbHeight, $width, $height);

            imageDestroy($image);
            break;
    }

    return $thumbImage;
}




/**
 * create thumbnail with GD
 *
 * @param	ressource	$image
 * @param 	string		$thumbFilename		Thumb file name
 *
 * @return  string | false 	path to thumbnail or false if error
 */
function lfm_applyGdImageAsThumbnail($image, $thumbFilename)
{
    $thumbPath = lfm_getThumbnailBasePath();

    imagesavealpha($image, true);

    $imageFormat  = strtolower(substr($thumbFilename, (strrpos($thumbFilename, '.') ? strrpos($thumbFilename, '.') + 1 : strlen($thumbFilename)), strlen($thumbFilename)));
    if ($imageFormat == 'jpg') {
        $imageFormat = 'jpeg';
    }

    if ($imageFormat === 'auto') {
        $imageFormat = lfm_getThumbnailBestImageFormat(imagesx($image), imagesy($image));
    }

    switch ($imageFormat) {
        case 'jpeg':
            imageinterlace($image, true);
            $result = @imagejpeg($image, $thumbPath . $thumbFilename, 90);
            break;
        case 'png':
        default:
            $result = @imagepng($image, $thumbPath . $thumbFilename);
            break;
    }


    imageDestroy($image);

    return $result ? $thumbFilename : false;

}







/**
 * create thumbnail with GD
 *
 * @param	ressource	$image
 * @param	int			$thumbWidth
 * @param	int			$thumbHeight
 * @param 	string		$thumbFilename		Thumb file name
 * @param   array		$effect_options
 *
 * @return  string | false 	path to thumbnail or false if error
 */
function lfm_createThumbnailFromGdImage($image, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options)
{
    $thumbImage = lfm_createGdThumbnailFromGdImage($image, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
    return lfm_applyGdImageAsThumbnail($thumbImage, $thumbFilename);
}














/**
 * Create a thumbnail with GD
 *
 * @param	string		$filename		Original file path
 * @param	array		$imageInfo
 * @param	int			$thumbWidth
 * @param	int			$thumbHeight
 * @param	string		$thumbFilename
 *
 * @return string | null | false	the thumb file name or null if the ressource of original file cannot be created or false if the thumbnail cannot be created
 */
function lfm_createGdThumbnail($filename, $imageInfo, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options)
{
    $image = lfm_createGdImageFromFilename($filename, $imageInfo);

    if (null === $image) {
        return null;
    }

    return lfm_createThumbnailFromGdImage($image, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
}








/**
 * Add film holes
 * @param	ressource	$image		GD Image
 * @return ressource	GD Image
 */
function lfm_addFilmHolesToGdImage($image)
{
    $th_width = @ImageSX($image);
    $th_height = @ImageSY($image);

    $filled = 0;
    $unfilled = $th_height;

    $filmholes_left = @ImageCreateFromPNG(lfm_getImagePath() . "filmholes_left.png");
    $filmholes_right = @ImageCreateFromPNG(lfm_getImagePath() . "filmholes_right.png");

    $fh_with = @ImageSX($filmholes_left);
    $fh_height = @ImageSY($filmholes_left);

    while ($unfilled > 0) {

        @ImageCopyResampled(
            $image, $filmholes_left,
            0, $filled, 0, 0,
            $fh_with, $fh_height, $fh_with, $fh_height
        );

        @ImageCopyResampled(
            $image, $filmholes_right,
            $th_width - $fh_with, $filled, 0, 0,
            $fh_with, $fh_height, $fh_with, $fh_height
        );


        $filled += $fh_height;
        $unfilled -= $fh_height;
    }

    return $image;
}





/**
 * Creates a thumbnail jpeg picture for the specified file and returns the full path to this picture.
 * return null 	if error on reading original image
 * return false if error on thumbnail creation
 *
 * @param string	$filename			The full path to the original file.
 * @param int		$thumbWidth			The maximum thumbnail width.
 * @param int		$thumbHeight		The maximum thumbnail height.
 * @param string	$thumbFilename		filename used in cache
 * @param bool		$slow				allow slow thumbnail processing
 * @param array		$effect_options
 *
 * @return string | null | false		the thumb file name or null if the ressource of original file cannot be created or false if the thumbnail cannot be created
 *
 */
function lfm_createThumbnailFromFile($filename, $thumbWidth, $thumbHeight, $thumbFilename, $slow, $effect_options)
{
    $thumbPath = lfm_getThumbnailBasePath();
    lfm_createDir($thumbPath);
    $addon = bab_getAddonInfosInstance('LibFileManagement');

    $imageInfo = @getImageSize($filename);

    if (!$imageInfo) {
        $mimeType = bab_getFileMimeType($filename);

        if ($mimeType === 'application/pdf' || $mimeType === 'image/svg+xml') {

            require_once dirname(__FILE__) . '/imagick.class.php';

            try {
                $im = new lfm_Imagick($filename.'[0]');
                $newim = $im->flattenImages(); // flatten to allow png transparency to jpeg white background on big images
                if ($newim instanceof Imagick)
                {
                    // if doc is correct we get true else we get a Imagick instance
                    $im = $newim;
                }

                if (!$im->writeImage($thumbPath . $thumbFilename)) {
                    $thumbFilename = null;
                }

                $im->clear();

            } catch (ImagickException $e) {
                $thumbFilename = null;
            }

            // open as GD image to apply effects
            $imageInfo = @getImageSize($thumbPath . $thumbFilename);
            return lfm_createGdThumbnail($thumbPath . $thumbFilename, $imageInfo, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
        }


        if (false !== strpos($mimeType, 'video')) {

            require_once dirname(__FILE__) . '/ffmpeg.class.php';

            $movie = new lfm_Ffmpeg_Movie($filename, false);
            $framenumber = round($movie->getFrameCount() * 0.01 * rand(25, 75));

            if ($framenumber > 1000) {
                $framenumber = 1000;
            }

            if (!is_object($frame = $movie->getFrame($framenumber))) {
                return null;
            }

            if (is_null($image = $frame->toGDImage())) {
                return null;
            }
            $image = lfm_createGdThumbnailFromGdImage($image, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);

            if ($thumbWidth > 80) {
                $image = lfm_addFilmHolesToGdImage($image);
            }

            return lfm_applyGdImageAsThumbnail($image, $thumbFilename);
        }


        if (0 === mb_strpos($mimeType, 'application/vnd.oasis.opendocument')) { // odt, odp ...

            if (!@copy('zip://'. $filename .'#Thumbnails/thumbnail.png', $addon->getUploadPath().'temp.png')) {
                return null;
            }

            $filename = $addon->getUploadPath().'temp.png';
            $imageInfo = @getImageSize($filename);

            return lfm_createGdThumbnail($filename, $imageInfo, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
        }


        if (0 === mb_strpos($mimeType, 'application/vnd.openxmlformats-officedocument')) { // pptx, docx, xlsx

            if (!@copy('zip://'. $filename .'#docProps/thumbnail.jpeg', $addon->getUploadPath().'temp.jpg')) {

                return null;
            }

            $filename = $addon->getUploadPath().'temp.jpg';
            $imageInfo = @getImageSize($filename);

            return lfm_createGdThumbnail($filename, $imageInfo, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
        }

        return null;
    }

    return lfm_createGdThumbnail($filename, $imageInfo, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
}





/**
 * Create thumbnail from data source
 *
 * @param bool		$slow				allow slow thumbnail processing (ignored)
 * @param	array	$effect_options
 *
 * @return string | false 	return thumb file name or false if error on writing thumbnail
 */
function lfm_createThumbnailFromData($filedata, $thumbWidth, $thumbHeight, $thumbFilename, $slow, $effect_options)
{
    if (is_resource($filedata['data'])) {
        $image = $filedata['data'];
    } else {
        $image = imagecreatefromstring($filedata['data']);
    }
    return lfm_createThumbnailFromGdImage($image, $thumbWidth, $thumbHeight , $thumbFilename, $effect_options);
}




/**
 * get RGB color for hexa string
 * @param	string	$hexcolor
 * @return	array
 */
function lfm_hexarr($color)
{
    $color = trim($color, '# ');

    if (6 === strlen($color)) {

        $r = substr($color, 0, 2);
        $g = substr($color, 2, 2);
        $b = substr($color, 4, 2);

    } elseif (3 === strlen($color)) {

        $r = str_repeat(substr($color, 0, 1), 2);
        $g = str_repeat(substr($color, 1, 1), 2);
        $b = str_repeat(substr($color, 2, 1), 2);

    } else {
        return array(0,0,0);
    }

    return array(
        hexdec($r),
        hexdec($g),
        hexdec($b)
    );
}








/**
 * Enter description here...
 *
 * @param 	string|NULL		$filename
 * @param	array|NULL		$filedata			alternative method, an array with 2 keys : data, lastupdate
 * @param 	int				$thumbWidth			The thumbnail width.
 * @param 	int				$thumbHeight		The thumbnail height.
 * @param	false|array		[$effect_options]	effect options
 * @param	bool			[$slow]				allow slow thumbnail processing
 *
 * @return string | null | false				the thumb file name or null if the ressource of original file is not readable or false if the thumbnail cannot be created.
 */
function lfm_getThumbnail($filename, $filedata, $thumbWidth, $thumbHeight, $effect_options = false, $slow = false)
{

    if (NULL !== $filename) {

        $basestring = $filename;
        $lastupdate = @filemtime($filename);

        if (!$lastupdate) {
            return null;
        }

        if ((!isset($effect_options['imageformat']) || $effect_options['imageformat'] === 'auto') && $imageInfo = @getImageSize($filename)) {

            if ('image/png' === $imageInfo['mime'] && (ord(@file_get_contents($filename, NULL, NULL, 25, 1)) == 6)) {
                /*
                  http://stackoverflow.com/questions/5495275/how-to-check-if-an-image-has-transparency-using-gd

                  The color type of PNG image is stored at byte offset 25. Possible values of that 25'th byte is:

                    0 - greyscale
                    2 - RGB
                    3 - RGB with palette
                    4 - greyscale + alpha
                    6 - RGB + alpha

                 */

                $effect_options['imageformat'] = 'png';
            }
        }

    } elseif (NULL !== $filedata) {

        if (is_resource($filedata['data'])) {
            $basestring = $filedata['lastupdate'];
        } else {
            $basestring = $filedata['data'];
            $lastupdate = bab_mktime($filedata['lastupdate']);
        }
        $lastupdate = bab_mktime($filedata['lastupdate']);
    } else {

        // no source?
        return null;
    }

    $thumbFilename = lfm_getThumbFilename($basestring, $thumbWidth, $thumbHeight, $effect_options, $lastupdate);
    if (lfm_thumbnailIsUpTodate($lastupdate, $thumbFilename)) {
        return $thumbFilename;
    }

    if (NULL !== $filename) {
        $thumbFilename = lfm_createThumbnailFromFile($filename, $thumbWidth, $thumbHeight, $thumbFilename, $slow, $effect_options);
    } else if (NULL !== $filedata) {
        $thumbFilename = lfm_createThumbnailFromData($filedata, $thumbWidth, $thumbHeight, $thumbFilename, $slow, $effect_options);
    }

    if (!empty($thumbFilename) && isset($effect_options['method'])) {

        switch($effect_options['method']) {

            case 'addDropShadow':

                lfm_addDropShadow(
                    lfm_getThumbnailBaseUrl() . $thumbFilename,
                    $effect_options['shadowcolor'],
                    $effect_options['backgroundcolor'],
                    $effect_options['percentopacity'],
                    $effect_options['offset']
                );

                break;

            case 'setPolaroid':
                lfm_setPolaroid(
                    lfm_getThumbnailBaseUrl() . $thumbFilename,
                    $effect_options['bordercolor'],
                    $effect_options['shadowcolor'],
                    $effect_options['backgroundcolor'],
                    $effect_options['angle']
                );
                break;


            case 'addBorder':
                lfm_addBorder(
                    lfm_getThumbnailBaseUrl() . $thumbFilename,
                    $effect_options['size'],
                    lfm_hexarr($effect_options['color']),
                    $effect_options['inner_size'],
                    lfm_hexarr($effect_options['inner_color'])
                );
                break;

        }
    }

    return $thumbFilename;
}







/**
 * Get icon name by mime type
 * @param	string	$mime
 * @return string
 */
function lfm_getIconNameByMime($mime)
{
    if (false !== strpos($mime, 'pdf')) {
        return 'x-office-document.png';
    }

    if (false !== strpos($mime, 'opendocument.text') || false !== strpos($mime, 'msword')) {
        return 'x-office-document.png';
    }

    if (false !== strpos($mime, 'presentation') || false !== strpos($mime, 'powerpoint')) {
        return 'x-office-presentation.png';
    }

    if (false !== strpos($mime, 'spreadsheet') || false !== strpos($mime, 'excel')) {
        return 'x-office-spreadsheet.png';
    }

    if (false !== strpos($mime, 'html')) {
        return 'text-html.png';
    }

    if (false !== strpos($mime, 'audio')) {
        return 'audio-x-generic.png';
    }

    if (false !== strpos($mime, 'image')) {
        return 'image-x-generic.png';
    }

    if (false !== strpos($mime, 'video')) {
        return 'video-x-generic.png';
    }

    if (false !== strpos($mime, 'zip') || false !== strpos($mime, 'tar') || false !== strpos($mime, 'rar')) {
        return 'package-x-generic.png';
    }


    return 'text-x-generic.png';
}




/**
 * Returns the url of a stock icon from mime type.
 *
 * @param	int		$size
 * @param 	string	$mime
 * @return 	string
 */
function lfm_stockIcon($size, $mime)
{
    $size = $size < 32 ? 16 : 32;

    $addon = bab_getAddonInfosInstance('LibFileManagement');
    $icons = $addon->getImagesPath().'icons/'.$size.'x'.$size.'/';

    return $icons . lfm_getIconNameByMime($mime);
}




/**
 * Get the url (img src) for an icon corresponding to the specified file.
 *
 */
class lfm_FileIcon
{
    private $filepath 	= null;
    private $width 		= 96;
    private $height 	= 64;
    private $thumb 		= null;
    private $icon 		= null;
    private $effects 	= null;

    /**
     *
     * @param string		$filepath
     * @param int			$width		The width in pixels of the thumbnail.
     * @param int			$height		The height in pixels of the thumbnail.
     * @return string
     */
    public function __construct($filepath, $width, $height)
    {
        $this->filepath = $filepath;
        $this->width = (int) $width;
        $this->height = (int) $height;
    }

    /**
     * Set effects options for thumbnail creation
     * @return lfm_FileIcon
     */
    public function setEffects($effects)
    {
        $this->effects = $effects;
        return $this;
    }


    /**
     * Get the thumb url or false
     * @return string | false
     */
    public function getThumbnail()
    {
        if (null === $this->thumb) {
            if ($thumbFilename = lfm_getThumbnail($this->filepath, null, $this->width, $this->height, $this->effects)) {
                $this->thumb = lfm_getThumbnailBaseUrl() . $thumbFilename;
            } else {
                $this->thumb = false;
            }
        }

        return $this->thumb;
    }


    /**
     * Get Url to an icon found from file mime type
     * @return string
     */
    public function getIcon()
    {
        if (null === $this->icon) {
            $this->icon = lfm_stockIcon($this->width, bab_getFileMimeType($this->filepath));
        }

        return $this->icon;
    }

    /**
     * If the file has a thumbnail the url of the thumbnail image is returned otherwise
     * the url to a stock icon corresponding to the filetype is returned.
     * @return string
     */
    public function __tostring()
    {
        $thumb = $this->getThumbnail();

        if (false !== $thumb) {
            return $thumb;
        }

        return $this->getIcon();
    }
}





/**
 * Get path for images
 * @return string
 */
function lfm_getImagePath()
{
    $addon = bab_getAddonInfosInstance('LibFileManagement');
    $ovidentiapath = dirname($_SERVER['SCRIPT_FILENAME']);
    return $ovidentiapath.'/'.$addon->getImagesPath();
}








/**
 * Add a drop shadow on a picture
 * @param	string	$filepath
 * @param	string	$shadowcolor
 * @param	string	$backgroundcolor
 * @param	int		$percentopacity
 * @param	int		$offset
 * @return 	boolean
 */
function lfm_addDropShadow($filepath, $shadowcolor, $backgroundcolor, $percentopacity, $offset)
{
    @lfm_system('convert "'.realpath($filepath).'" \( +clone -background "'.$shadowcolor.'" -shadow '.$percentopacity.'x3+'.$offset.'+'.$offset.' \) +swap -background "'.$backgroundcolor.'" -layers merge  +repage "'.realpath($filepath).'"', $retval);


    if ($retval !== 0) {
        // if imageMagick is not supported, try with php

        require_once dirname(__FILE__).'/class.dropshadow.php';

        $ds = new lfm_dropShadow();

        $ds->setShadowPath(lfm_getImagePath());
        $ds->loadImage($filepath);

        $ds->applyShadow($backgroundcolor);
        $return = $ds->saveShadowedImage($filepath);

        return $return;
    }

    return true;
}


/**
 * Add a drop shadow on a picture
 * @param	string		$filepath
 * @param	string		$bordercolor
 * @param	string		$shadowcolor
 * @param	string		$backgroundcolor
 * @param	int|false	$angle
 *
 * @return 	boolean
 */
function lfm_setPolaroid($filepath, $bordercolor, $shadowcolor, $backgroundcolor, $angle = false)
{
    if (false === $angle) {
        $polaroid = '+polaroid';
    } else {
        $polaroid = '-polaroid '.$angle;
    }

    @lfm_system('convert "'.realpath($filepath).'" -bordercolor "'.$bordercolor.'" -background "'.$shadowcolor.'" '.$polaroid.' -background "'.$backgroundcolor.'" -flatten  "'.realpath($filepath).'"', $retval);

    if ($retval !== 0) {
        // if imageMagick is not supported, fallback to dropshadow

        return lfm_addDropShadow($filepath, $shadowcolor, $backgroundcolor, 60, 5);
    }

    return true;
}







/**
 * Add a border on a picture
 * @param	string		$filepath
 * @param	int			$size
 * @param	array		$color				border color	RGB
 * @param	int			$size_internal
 * @param	array		$color_internal		border color	RGB
 *
 * @return 	boolean
 */
function lfm_addBorder($filepath, $size, $color, $size_internal, $color_internal)
{
    if (!$size && !$size_internal) {
        return true;
    }

    $mimeType = bab_getFileMimeType($filepath);

    switch ($mimeType) {
        case 'image/gif':
            if (imagetypes() & IMG_GIF) {
                $im = imageCreateFromGif($filepath);
            }
            break;
        case 'image/jpeg':
            if (imagetypes() & IMG_JPG)  {
                $im = imageCreateFromJpeg($filepath);
            }
            break;
        case 'image/png':
            if (imagetypes() & IMG_PNG)  {
                $im = imageCreateFromPng($filepath);
            }
            break;
        case 'image/wbmp':
            if (imagetypes() & IMG_WBMP)  {
                $im = imageCreateFromWbmp($filepath);
            }
            break;
    }

    $width=ImageSx($im);
    $height=ImageSy($im);

    $width_internal = $width + (2*$size_internal);
    $height_internal = $height + (2*$size_internal);
    $img_adj_width= (2*$size) + $width_internal;
    $img_adj_height= (2*$size) + $height_internal;

    $newimage = imagecreatetruecolor($img_adj_width, $img_adj_height);
    $border_color = imagecolorallocate($newimage, $color[0], $color[1], $color[2]);
    $border_color_internal = imagecolorallocate($newimage, $color_internal[0], $color_internal[1], $color_internal[2]);

    imagefilledrectangle($newimage,0,0,$img_adj_width, $img_adj_height, $border_color);
    imagefilledrectangle($newimage,$size,$size,$width_internal + $size -1, $height_internal + $size -1, $border_color_internal);

    imageCopyResized($newimage, $im, $size + $size_internal, $size + $size_internal,0,0,$width,$height,$width,$height);
    ImagePng($newimage,$filepath);

    return true;
}


/**
 * Get up to date thumb filename or null if the thumbnail does not exists
 * @param string $filepath
 * @param int 		$width
 * @param int 		$height
 * @param array	 	$effect_options
 * @return string
 */
function lfm_getUpToDateThumbUrl($filepath, $width, $height, $effect_options)
{
    if (!is_file($filepath)) {
        return null;
    }

    $lastupdate = @filemtime($filepath);

    if (!$lastupdate) {
        return null;
    }

    $thumbFilename = lfm_getThumbFilename($filepath, $width, $height, $effect_options, $lastupdate);

    if (lfm_thumbnailIsUpTodate($lastupdate, $thumbFilename)) {
        return lfm_getThumbnailBaseUrl().$thumbFilename;
    }

    return null;
}



class lfm_callThumbnail
{
    /**
     * if thumbnail exists, return thumbnail url
     * else store informations in session and return an url to thumbnailer
     *
     * @param string 	$filepath
     * @param int 		$width
     * @param int 		$height
     * @param array	 	$effect_options
     * @return string
     */
    public function getUrl($filepath, $width, $height, $effect_options)
    {
        $url = lfm_getUpToDateThumbUrl($filepath, $width, $height, $effect_options);

        if (null !== $url) {
            return $url;
        }

        $uid = uniqid();

        $_SESSION['LibFileManagement']['thumbnailer'][$uid] = array(
            'filepath' 			=> $filepath,
            'width'				=> $width,
            'height'			=> $height,
            'effect_options'	=> $effect_options
        );

        $addon = bab_getAddonInfosInstance('LibFileManagement');
        return $addon->getUrl().'thumbnail&uid='.$uid;
    }


    public function create($uid)
    {
        if (!isset($_SESSION['LibFileManagement']['thumbnailer'][$uid])) {
            return null;
        }

        $arr = $_SESSION['LibFileManagement']['thumbnailer'][$uid];

        $thumbfilename = lfm_getThumbnail($arr['filepath'], null, $arr['width'], $arr['height'], $arr['effect_options'], true);

        if (false === $thumbfilename) {
            bab_debug(sprintf('cannot write thumbnail image file in %s', lfm_getThumbnailBasePath()), DBG_WARNING, 'thumbnailer');
            return false;
        }

        if (null === $thumbfilename) {
            bab_debug(sprintf('cannot open original image file %s', $arr['filepath']), DBG_WARNING, 'thumbnailer');
            return null;
        }

        $path = new bab_Path(lfm_getThumbnailBasePath() . $thumbfilename);
        bab_downloadFile($path);
    }
}



if ($uid = bab_gp('uid'))
{
    $obj = new lfm_callThumbnail;
    $obj->create($uid);
}
