;<?php/*

[general]
name                        ="LibFileManagement"
version                     ="0.4.1"
encoding                    ="UTF-8"
description                 ="Ovidentia Library for file management"
description.fr              ="Librairie partagée fournissant des outils de manipulation sur les fichiers"
long_description.fr         ="README.md"
delete                      =1
ov_version                  ="7.9.0"
php_version                 ="5.0.0"
addon_access_control        ="0"
author                      ="Paul de Rosanbo (paul.derosanbo@cantico.fr)"
mod_gd2                     ="Available"
upload_directory            ="Available"
preinstall_script           ="preinstall.php"
icon                        ="document-open.png"
mysql_character_set_database="latin1,utf8"
configuration_page          ="configuration"
tags                        ="library,default"

[recommendations]
upload_max_file_size=30M

;*/?>